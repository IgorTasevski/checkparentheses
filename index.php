<?php

function checkForParenthesis($string) {
    $checkForCurlyBrackets = array('}' => '{', ']' => '[', ')' => '(');
    $checkForParentheses = array();
    foreach (str_split($string) as $character) {
        switch ($character) {
            case '{':
            case '[':
            case '(':
                $checkForParentheses[] = $character;
                break;
            case '}':
            case ']':
            case ')':
                if (!count($checkForParentheses) || array_pop($checkForParentheses) != $checkForCurlyBrackets[$character]) {
                    return false;
                    break;
                }
            default:
                break;
        }
    }
    return count($checkForParentheses) === 0;
}

function check_balanced($string) {
    echo "$string - <b> does " . (checkForParenthesis($string) ? '' : 'not ') . "have parentheses\n </b>";
}

check_balanced("public function test() {{echo 'hey'}");
echo "<br/>";
check_balanced("public function check() {echo 'hey'}");
echo "<br/>";
check_balanced("{([]){]()}");
echo "<br/>";
check_balanced("public function check()) }}{");
